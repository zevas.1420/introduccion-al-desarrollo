from farmaco.models import Drogas, Drogueria

def cargar_droga_base(nombre,cantidad,drogueria_id):
    lista_nombres = Drogas.objects.all()

    for item in lista_nombres:
        if item.nombre == nombre:
            raise Exception("Error el nombre ya existe")

    droga = Drogas() #crear objecto
    droga.nombre = nombre
    droga.cantidad = cantidad
    droga.nombre_drogue_id = drogueria_id
    droga.save()

def cargar_drogueria_base(nombre):
    lista_nombres_dr = Drogueria.objects.all()

    for item in lista_nombres_dr:
        if item.nombre == nombre:
            raise Exception("Error el nombre de la drogueria ya existe")

    drogueria = Drogueria() #crear objecto
    drogueria.nombre = nombre
    drogueria.save()

