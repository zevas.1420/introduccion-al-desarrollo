from django.shortcuts import render

from farmaco.models import Drogas, Drogueria
from farmaco.helpers.cargas import cargar_droga_base, cargar_drogueria_base #es donde tomamos las excepciones

# Create your views here.

def cargar_droga(request):
    #LISTADO DE DROGAS
    lista_droga = Drogas.objects.all() #listado de todas las drogas
    lista_drogueria = Drogueria.objects.all()  # listado de todas las droguerias

    # condicional para que sea modo POST
    if request.method == 'POST':
        try:
            nombre = request.POST.get('nombre_html') #nombre es una variable del html value o name
            cantidad = request.POST.get('cantidad_html')
            drogueria_id = request.POST.get('drogueria_id')
            cargar_droga_base(nombre,cantidad,drogueria_id)

        except Exception as err:
            return render(request, 'cargar_droga.html',
                          {'lista_droga':lista_droga,'lista_drogueria':lista_drogueria, 'error':str(err)})

    return render(request, 'cargar_droga.html',{'lista_droga':lista_droga,'lista_drogueria':lista_drogueria,})

def cargar_drogueria(request):

    lista_drogueria = Drogueria.objects.all()  # lista de droguerias }

    if request.method == 'POST':
        try:
            nombre = request.POST.get('nombre')
            cargar_drogueria_base(nombre)
        except Exception as err:
            return render(request,'cargar_drogueria.html',{'lista_drogueria':lista_drogueria,'error':str(err)}) #llaves

    return render(request, 'cargar_drogueria.html',{'lista_drogueria': lista_drogueria})





