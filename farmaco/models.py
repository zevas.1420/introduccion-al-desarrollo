from django.db import models

# Create your models here.
class Drogueria(models.Model):
    nombre = models.CharField(max_length=100)

class Drogas(models.Model):
    nombre = models.CharField(max_length=100)
    cantidad = models.IntegerField()
    nombre_drogue = models.ForeignKey(Drogueria, on_delete=models.CASCADE, related_name='drogueria')


