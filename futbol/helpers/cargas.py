from futbol.models import Futbolista


def cargar_jugador_base(nombre, numero):
    lista_jugador = Futbolista.objects.all()
    if nombre == "":
        raise Exception("El nombre no puede estar vacio")

    # obtengo los datos del request
    for item in lista_jugador:
        if item.numero == int(numero):
            raise Exception("Error el numero ya existe")

    if int(numero) > 11:
        raise Exception("Error personaje no puede ser mayor a 11")
    # creo el nuevo personaje
    futbolista = Futbolista()
    futbolista.nombre = nombre
    futbolista.numero = numero
    futbolista.save()
