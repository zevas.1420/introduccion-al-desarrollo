from django.shortcuts import render
from django.db import IntegrityError

from futbol.models import Futbolista
from futbol.helpers.cargas import cargar_jugador_base

# Create your views here.

def cargar_jugador(request):
    lista_futbolista = Futbolista.objects.all()
    lista_futbolista_count = Futbolista.objects.all().order_by('numero')

    if request.method == 'POST':
        try:
            nombre = request.POST.get('nombre')
            numero = request.POST.get('numero')
            cargar_jugador_base(nombre,numero)
        except IntegrityError as otroErr:
            err = 'El nombre no puede estar repetido'
            return render(request, 'jugador_carga.html',
                          {'lista_futbolista': lista_futbolista,'lista_futbolista_count':lista_futbolista_count, 'error': str(err)})
        except Exception as err:
            return render(request, 'jugador_carga.html',
                          {'lista_futbolista': lista_futbolista,'lista_futbolista_count':lista_futbolista_count, 'error': str(err)})

    return render(request, 'jugador_carga.html',
                  {'lista_futbolista': lista_futbolista, 'lista_futbolista_count':lista_futbolista_count})

def inicio(request):
    return render(request, 'index.html')
