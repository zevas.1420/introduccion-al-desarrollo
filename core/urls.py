"""IntroDesarrollo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from heroes import views as views_h
from futbol import views as views_f

from farmaco import views as views_farma

urlpatterns = [
    path('admin/', admin.site.urls),
    path('juego/profesion', views_h.cargar_profesion),
    path('juego/habilidad', views_h.cargar_habilidad),
    path('juego/heroe', views_h.cargar_personaje),
    path('futbol/index', views_f.inicio),
    path('futbol/jugador', views_f.cargar_jugador),
    path('droga/creardroga', views_farma.cargar_droga),
    path('droga/creardrogueria', views_farma.cargar_drogueria),
]
