from django.shortcuts import render
from heroes.models import Personaje, Profesion, Habilidades

# Create your views here.

def cargar_profesion(request):
    if request.method == 'POST':
        nombre = request.POST.get('nombre') #le doy el nombre traido por el form
        profesion = Profesion() #creo objeto
        profesion.nombre = nombre #seteo el nombre
        profesion.save() #guardo profesion en sql

    lista = Profesion.objects.all() #listo todas las profesiones
    return render(request, 'profesion_carga.html',{'lista':lista})

def cargar_habilidad(request):
    if request.method == 'POST':
        nombre = request.POST.get('nombre') #nombre de habilidad
        profesion_id = request.POST.get('profesion') #cargo ID de profesion
        nivel = request.POST.get('nivel') #cargo nivel
        habilidad = Habilidades()
        habilidad.nombre = nombre
        habilidad.profesion_id = profesion_id
        habilidad.nivel_min = nivel
        habilidad.save()

    lista_prof = Profesion.objects.all()  # listo todas las profesiones
    lista_hab = Habilidades.objects.all()  # listo todas las profesiones
    return render(request, 'habilidad_carga.html', {'lista_hab':lista_hab,'lista_prof':lista_prof})

def cargar_personaje(request):
    lista_personaje = Personaje.objects.all()
    lista_profesion = Profesion.objects.all()
    if request.method == 'POST':
        nombre = request.POST.get('nombre')
        profesion_id = request.POST.get('profesion')
        nivel = request.POST.get('nivel')
        if int(nivel) > 99:
            error = "Error personaje no puede ser mayor a 99"
            return render(request, 'heroe_carga.html', {'lista_personaje': lista_personaje, 'lista_profesion': lista_profesion, 'error':error})
        personaje = Personaje()
        personaje.nombre = nombre
        personaje.tipo_id = profesion_id
        personaje.nivel = nivel
        personaje.save()

    return render(request, 'heroe_carga.html', {'lista_personaje': lista_personaje, 'lista_profesion': lista_profesion})

