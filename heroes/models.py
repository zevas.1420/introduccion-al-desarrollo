from django.db import models
from django.utils import timezone

class Profesion(models.Model):
    nombre = models.CharField(max_length=200)

class Habilidades(models.Model):
    nombre = models.CharField(max_length=200)
    nivel_min = models.IntegerField()
    profesion = models.ForeignKey(Profesion, on_delete=models.CASCADE, related_name='habilidades')

class Personaje(models.Model):
    nombre = models.CharField(max_length=100)
    nivel = models.IntegerField()
    tipo = models.ForeignKey(Profesion, on_delete=models.CASCADE, related_name='personaje')
